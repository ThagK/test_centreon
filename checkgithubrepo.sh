#!/bin/bash

#if git --version >/dev/null 2>&1
#then
#	echo "OK"
#else
#	echo "Git n'est pas installé"
#	exit 1
	
#fi

#Lister les dépôts distants de centreon en utilisant l'API github
#Ensuite parser le JSON pour enregistrer les éléments full_name et description dans un fichier

curl --silent "https://api.github.com/users/centreon/repos" | jq '.[] | {name: .full_name, description: .description}' > liste_repo_dist

#Enlever les accolades
#sed 's/{//' liste_repo_dist > liste_repo_dist

#Test pour voir à la fin du script si les infos sont bien dans le fichier
cat liste_repo_dist


#Lister tous les repositories du dépôt distant (ou add + show ?)
#Doit faire pour chaque repo distant
#git ls-remote https://github.com/centreon/centreon > liste_repo_dist
#cat liste_repo_dist
