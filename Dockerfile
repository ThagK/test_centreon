FROM centos:8

RUN yum install -y jq
RUN yum install -y rpmdevtools

#PARTIE BUILD SPEC FILE

#Mettre en place l'arbo ~/rpmbuild/
RUN rpmdev-setuptree

#Copier le script dans /SOURCES et le SPEC FILE dans /SPECS
COPY checkgithubrepo.sh /root/rpmbuild/SOURCES/
COPY checkgithubrepo.spec /root/rpmbuild/SPECS/

#Build le RPM spec
RUN rpmbuild -bb /root/rpmbuild/SPECS/checkgithubrepo.spec
#Vérifier que le paquet a bien été construit
RUN ls -lrt /root/rpmbuild/RPMS/noarch

#Exécuter le RPM au démarrage du conteneur
CMD rpm -i  /root/rpmbuild/RPMS/noarch/checkgithubrepo-1.0-1.el8.noarch.rpm

