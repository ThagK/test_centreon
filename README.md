# Test_centreon


1. Docker
   
    - Installation
        Suivi pas à pas de la doc : https://docs.docker.com/engine/install/centos/
	    Rajout "--allowerasing" pour permettre de remplacer les paquets en conflit et installer docker engine.
	    Puis commande "sudo systemctl enable docker" pour la remontée du service au reboot
	    Pour simplifier, je n'ai pas créé de group/user docker
        Rmq : sudo docker info => connaître les infos comme répertoire de stockage sur le host

    - Dockerfile : 
	    https://docs.docker.com/get-started/part2/
        https://docs.docker.com/develop/develop-images/dockerfile_best-practices/
	    https://docs.docker.com/engine/reference/builder/ (pour comprendre les commandes)
	    https://docs.docker.com/develop/develop-images/multistage-build/ (pour dockerfile avec git et rpm package)

    - Quelques erreurs rencontrées !
        Le conteneur n'accédait plus à internet 
	    => lors du debug, bien vérifier qu'il a accès avec un docker run hello-world ou autre
	    => NE PAS FAIRE docker system prune -a


2. Git
- Image docker : https://hub.docker.com/r/alpine/git

- Script :
	Utilisation de jq pour parser le JSON obtenu en utilisant l'API github. 
	J'ai choisi de récupérer la liste de cette manière car c'était la façon de récupérer tous les dépôts distants avec une seule commande.
		Problème rencontré : il semble que certains dépôts distants ne sont pas listés, mais je n'arrive pas à trouver pourquoi.
		
- Connection avec gitlab fait via http car ssh ne fonctionne pas malgé plusieurs tentatives :
    https://forum.gitlab.com/t/permission-denied-publickey/29670
    https://docs.github.com/en/free-pro-team@latest/github/using-git/changing-a-remotes-url

3. RPM
- Doc :
    https://wiki.centos.org/HowTos/SetupRpmBuildEnvironment
    https://rpm-packaging-guide.github.io/#what-is-a-spec-file
    https://www.linuxtricks.fr/wiki/centos-fedora-creer-des-rpm
    https://developer.ibm.com/technologies/linux/tutorials/l-rpm1/

Debug erreur dans %prep : https://serverfault.com/questions/311440/build-rpm-using-source-directory-not-tarball
Inspecter paquet : https://blog.packagecloud.io/eng/2015/10/13/inspect-extract-contents-rpm-packages/
Explication buildroot (par rapport à erreur dans %files qui ne trouvais pas le fichier dans répertoire en dehors de buildroot) : 
	https://stackoverflow.com/questions/8084796/what-actually-is-rpm-build-root

- Etapes :
	Installation du paquet : yum install rpmdevtools
	rpmdev-setuptree (mise en place de l'arbo dans ~/rpmbuild)
	mettre script dans ~/rpmbuild/SOURCES
	dans ~/rpmbuild/SPECS 
		faire "rpmdev-newspec checkgithubrepo" pour créer nouveau fichier spec
	"rpmbuild --eval <nom_macro>" (ex: %buildroot) pour savoir à quoi les macros renvoient

Fichier du script généré dans /BUILD

(Image docker : https://hub.docker.com/r/rpmbuild/centos7/)

5. Jenkins + Docker
- Image Jenkins : https://hub.docker.com/r/jenkins/jenkins

- Etapes :
    Dans config.xml => <usecurity>false</usecurity> pour contourner le pb lors de la connexion à la console après A/R jenkins
    Le job contient la configuration d'un pipeline qui écoute sur gitlab. Lors d'un commit, Jenkins va lancer le Jenkinsfile contenu dans le repository
    Le Jenkinsfile va lire le Dockerfile, faire le build de l'image puis lancer le conteneur.
    Ensuite, les actions décrites dans "stages" sont effectuées dans ce conteneur, qui sera ensuite supprimé.

**ETAPES CONFIGURATION JOB :**
1. Bien vérifier que les plugins par défaut (contiennent git) sont installés, puis installer les plugins docker pipeline et docker commons
2. Nouveau job -> Pipeline
3. Section "Build Triggers" -> Sélectionner "Scrutation de l'outil de gestions de versions" (en anglais "Polling SCM" je crois, c'est la dernière option) -> Mettre * * * * * pour signifier à Jenkins de scruter toutes les minutes
4. Section "Pipeline" -> Definition = Pipeline script from SCM -> SCM = Git -> Repository URL : l'url de mon dépôt (credentials : aucun)
5. Garder le reste comme par défaut
6. Rajouter "jenkins" dans le groupe Docker afin d'avoir la permission de lancer Dockerfile : sudo usermod -aG docker jenkins


- Documents
    Pipeline pour lancer dockerfile : https://www.jenkins.io/doc/book/pipeline/docker/#dockerfile
    Jenkinsfile : https://www.jenkins.io/doc/book/pipeline/jenkinsfile/

Je n'ai pas réussi à configurer un conteneur jenkins avec le job de configuré. Documentation :
    Dockerfile avec Jenkins configuré en code :
    https://www.digitalocean.com/community/tutorials/how-to-automate-jenkins-setup-with-docker-and-jenkins-configuration-as-code
    https://plugins.jenkins.io/configuration-as-code/
    https://plugins.jenkins.io/job-dsl

    Configurer un pipeline en code : https://github.com/jenkinsci/configuration-as-code-plugin/blob/master/demos/jobs/pipeline.yaml
    https://www.jenkins.io/doc/book/pipeline/syntax
