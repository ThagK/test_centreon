Name:           checkgithubrepo
Version:        1.0 
Release:        1%{?dist}
Summary:        Script shell listant les dépôts distants de l'organisation Centreon

License:        GPLv3+
URL:            https://example.com/%{name}
Source0:        %{_topdir}/SOURCES/%{name}.sh

Requires:       bash

BuildArch:	noarch

%description
Ce package lance un script shell listant les dépôts distants github de l'organisation Centreon

%prep


%build


%install
cp %{_topdir}/SOURCES/%{name}.sh %{buildroot}/
chmod +x %{buildroot}/%{name}.sh
%{buildroot}/%{name}.sh
cp %{_topdir}/BUILD/liste_repo_dist /liste_repo_dist
chmod +x /liste_repo_dist


%post
cat /liste_repo_dist

%files
/%{name}.sh


%changelog
* Tue Dec  8 2020 thag
- Package du script github
